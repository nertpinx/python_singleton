""" Stupid module which just sets a global state variable """

from .state import STATE


def test():
    """ The only function which prints a STATE.word """
    STATE.word = 'Hello'
    # Uncomment the following to check that pylint will catch the error
    #STATE.word_with_typo = 'Hello'
