"""
The global state module, ideally the State would not be importable, just
the STATE object
"""

# R0903: Why public methods when just trying something out, right?
# R0205: This needs to work in python2 for a while as well
# pylint: disable=R0903,R0205
class State(object):
    """
    The global state, should not be used through a class, but rather the global
    object STATE (this way we don't need to create singletons
    """

    __slots__ = ['_word']

    def __init__(self):
        self._word = 'Blah'

    @property
    def word(self):
        """ The actual stored word """
        return self._word

    @word.setter
    def word(self, word: str):
        self._word = word

    @word.deleter
    def word(self):
        del self._word


STATE = State()
