""" Stupid module which just prints a variable from the global state """

from .state import STATE


def test():
    """ The only function which prints a STATE.word """
    print(STATE.word)
    # Uncomment the following to check that pylint will catch the error
    #print(STATE.word_with_typo)
