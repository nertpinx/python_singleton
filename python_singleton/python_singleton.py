""" Just having "fun" with some global states """

from . import fdsa
from . import asdf
from . import meh


def main():
    """ Is there anything easier/more complex that I should check? """
    fdsa.test()
    asdf.test()
    meh.test()
