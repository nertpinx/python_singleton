""" Stupid module which just deletes a variable from the global state """

from .state import STATE


def test():
    """ The only function which just deletes a STATE.word """
    del STATE.word
    # Uncomment the following to check that pylint will catch the error
    #del STATE.word_with_typo
