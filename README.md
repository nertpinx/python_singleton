# Testing some stupid stuff

What should work:

 - `./setup.py develop` (in venv)
 - `tox`
 
What should **not** work:

 - Running `tox` after uncommenting lines with typos (in `asdf.py` and/or `fdsa.py`)
