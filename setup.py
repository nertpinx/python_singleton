#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

meta = {}
exec(open('meta.py').read(), meta)

setup(
    name=meta.get('NAME'),
    version=meta.get('VERSION'),
    description=meta.get('DESCRIPTION'),
    keywords=meta.get('KEYWORDS'),
    author=meta.get('AUTHOR'),
    author_email=meta.get('EMAIL'),
    license=meta.get('LICENSE'),
    packages=find_packages(),
    entry_points={
        'console_scripts': [
            'python-singleton = python_singleton.python_singleton:main'
        ]
    },
)
